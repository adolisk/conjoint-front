export default {
    state() {
        return {
            classifiers: []
        }
    },
    getters() {},
    mutations: {
        /**
         * Обновим массив таблицы данных
         * @param state
         * @param payload
         * @constructor
         */
        SET_CLASSIFIERS(state, payload) {
            state.classifiers.splice(0, state.classifiers.length, ...payload);
        },

        /**
         * Обновим конкретную запись по ID
         * @param state
         * @param payload
         * @constructor
         */
        UPDATE_CLASSIFIERS_BY_ID(state, payload) {
            const index = state.classifiers.findIndex( (element) => element.id === payload.id);
            Object.keys(payload).forEach( key => {
                state.classifiers[index][key] = payload[key];
            })
        },
    },
    actions: {
        /**
         * Первоначальная инициализация classifiers
         *
         * @param commit
         * @param $axios
         * @returns {Promise<void>}
         */
        async nuxtServerInit({ commit }, { $axios }) {
            const { data } = await $axios.$get(process.env.API_URL + '/api/classifiers');
            commit('SET_CLASSIFIERS', data);
        },

        /**
         *  Обновим состояние classifiers
         *
         * @param commit
         * @param data
         * @returns {Promise<void>}
         */
        async updateClassifiersList({ commit }, data) {
            commit('SET_CLASSIFIERS', data);
        },

        /**
         * экшен для обновления конкретной записи
         * @param commit
         * @param data
         */
        textUpdate({ commit }, data) {
            commit('UPDATE_CLASSIFIERS_BY_ID', data);
        }
    }
}
