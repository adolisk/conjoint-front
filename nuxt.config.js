const env = require('dotenv').config();

module.exports = {
  env:env.parsed,
  /*
  ** Headers of the page
  */
  head: {
    title: 'conjoint',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'classifier | conjoint&apos;s test task' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel:'stylesheet', href:'https://fonts.googleapis.com/css?family=Fira+Sans:200,400,800&subset=cyrillic'},
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  css:[
    '~/assets/scss/app.scss',
  ],
  modules: [
      '@nuxtjs/axios',
  ],
  vendor:[
      'element-ui'
  ],
  plugins:[
      { src: '~plugins/element-ui', ssr: true }
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

