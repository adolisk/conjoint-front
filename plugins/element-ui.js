import Vue from 'vue'
import ElementUi from 'element-ui'
import locale from 'element-ui/lib/locale/lang/ru-RU';

export default () => {
    Vue.use(ElementUi, { locale });
}
